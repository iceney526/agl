using Agl.DataSource;
using Agl.PetSorters;
using Microsoft.Extensions.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Agl.Tests
{
    [TestClass]
    public class CatSorterTests
    {
        private CatSorter _catSorter;
        private ILogger<CatSorter> _logger;
        private IDataSource _dataSource;

        [TestInitialize]
        public void TestInitialize()
        {
            _logger = Substitute.For<ILogger<CatSorter>>();
            _dataSource = Substitute.For<IDataSource>();
            _catSorter = new CatSorter(_logger, _dataSource);
        }

        [TestMethod]
        public async Task CatSorter_MergeOwnerCats()
        {
            _dataSource.GetPetOwnershipAsync().Returns(Task.FromResult(new Models.PetOwnership 
            {
                Owners = new List<Models.Owner>
                {
                    new Models.Owner 
                    { 
                        Gender = "Male", 
                        Pets = new List<Models.Pet> 
                        { 
                            new Models.Pet { Name = "CatA", Type = "Cat" }
                        }
                    },
                    new Models.Owner
                    {
                        Gender = "Male",
                        Pets = new List<Models.Pet>
                        {
                            new Models.Pet { Name = "CatB", Type = "Cat" }
                        }
                    }
                }
            }));

            var genderCatSets = await _catSorter.Sort();

            Assert.IsNotNull(genderCatSets);
            Assert.AreEqual(1, genderCatSets.Count);
            Assert.AreEqual("Male", genderCatSets[0].OwnderGender);
            Assert.AreEqual(2, genderCatSets[0].CatNames.Count);
            Assert.AreEqual("CatA", genderCatSets[0].CatNames[0]);
            Assert.AreEqual("CatB", genderCatSets[0].CatNames[1]);
        }

        [TestMethod]
        public async Task CatSorter_GroupByOwnerGender()
        {
            _dataSource.GetPetOwnershipAsync().Returns(Task.FromResult(new Models.PetOwnership
            {
                Owners = new List<Models.Owner>
                {
                    new Models.Owner
                    {
                        Gender = "Male",
                        Pets = new List<Models.Pet>
                        {
                            new Models.Pet { Name = "CatA", Type = "Cat" }
                        }
                    },
                    new Models.Owner
                    {
                        Gender = "Female",
                        Pets = new List<Models.Pet>
                        {
                            new Models.Pet { Name = "CatB", Type = "Cat" }
                        }
                    },
                    new Models.Owner
                    {
                        Gender = "Male",
                        Pets = new List<Models.Pet>
                        {
                            new Models.Pet { Name = "CatC", Type = "Cat" }
                        }
                    },
                    new Models.Owner
                    {
                        Gender = "Female",
                        Pets = new List<Models.Pet>
                        {
                            new Models.Pet { Name = "CatD", Type = "Cat" }
                        }
                    },
                    new Models.Owner
                    {
                        Gender = "Male",
                        Pets = null
                    }
                }
            }));

            var genderCatSets = await _catSorter.Sort();

            Assert.IsNotNull(genderCatSets);
            Assert.AreEqual(2, genderCatSets.Count);
            Assert.AreEqual("Male", genderCatSets[0].OwnderGender);
            Assert.AreEqual("Female", genderCatSets[1].OwnderGender);
            Assert.AreEqual(2, genderCatSets[0].CatNames.Count);
            Assert.AreEqual("CatA", genderCatSets[0].CatNames[0]);
            Assert.AreEqual("CatC", genderCatSets[0].CatNames[1]);
            Assert.AreEqual(2, genderCatSets[1].CatNames.Count);
            Assert.AreEqual("CatB", genderCatSets[1].CatNames[0]);
            Assert.AreEqual("CatD", genderCatSets[1].CatNames[1]);
        }

        [TestMethod]
        public async Task CatSorter_FilterPets()
        {
            _dataSource.GetPetOwnershipAsync().Returns(Task.FromResult(new Models.PetOwnership
            {
                Owners = new List<Models.Owner>
                {
                    new Models.Owner
                    {
                        Gender = "Male",
                        Pets = new List<Models.Pet>
                        {
                            new Models.Pet { Name = "CatA", Type = "Cat" }
                        }
                    },
                    new Models.Owner
                    {
                        Gender = "Male",
                        Pets = new List<Models.Pet>
                        {
                            new Models.Pet { Name = "CatB", Type = "Cat" },
                            new Models.Pet { Name = "DogA", Type = "Dog" }
                        }
                    },
                    new Models.Owner
                    {
                        Gender = "Male",
                        Pets = new List<Models.Pet>
                        {
                            new Models.Pet { Name = "CatC", Type = "Cat" },
                            new Models.Pet { Name = "FishA", Type = "Fish" },
                        }
                    }
                }
            }));

            var genderCatSets = await _catSorter.Sort();

            Assert.IsNotNull(genderCatSets);
            Assert.AreEqual(1, genderCatSets.Count);
            Assert.AreEqual("Male", genderCatSets[0].OwnderGender);
            Assert.AreEqual(3, genderCatSets[0].CatNames.Count);
            Assert.AreEqual("CatA", genderCatSets[0].CatNames[0]);
            Assert.AreEqual("CatB", genderCatSets[0].CatNames[1]);
            Assert.AreEqual("CatC", genderCatSets[0].CatNames[2]);
        }
    }
}
