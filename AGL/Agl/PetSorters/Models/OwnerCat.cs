﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Agl.PetSorters.Models
{
    public class GenderCats
    {
        public string OwnderGender { get; set; }
        public List<string> CatNames { get; set; }
    }
}
