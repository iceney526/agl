﻿using Agl.DataSource;
using Agl.Models;
using Agl.PetSorters.Models;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Runtime.InteropServices.ComTypes;
using System.Text;
using System.Threading.Tasks;

namespace Agl.PetSorters
{
    /// <summary>
    /// The CatSorter class fitlers and sorts cats from a pet data source.
    /// </summary>
    public class CatSorter
    {
        private ILogger<CatSorter> _logger;
        private IDataSource _dataSource;

        /// <summary>
        /// Constructs a CatSorter instance using an ILogger object and the data source instance.
        /// </summary>
        /// <param name="logger">The ILogger instance.</param>
        /// <param name="dataSource">The IDataSource instance.</param>
        public CatSorter(ILogger<CatSorter> logger, IDataSource dataSource)
        {
            _logger = logger;
            _dataSource = dataSource;
        }

        /// <summary>
        /// Filters and sorts the data source where owners and pets information is provided.
        /// </summary>
        /// <returns>Returns a list of all the cats in alphabetical order under a heading of the gender of their owner.</returns>
        public async Task<List<GenderCats>> Sort()
        {
            _logger.LogInformation("Get pet ownership data.");
            var petOwnership = await _dataSource.GetPetOwnershipAsync();

            _logger.LogInformation("Extract cats information and group cat entries by owner gender.");
            var catGroupings = petOwnership
                                .Owners
                                .SelectMany(owner => owner.Pets ?? new List<Pet>(), (owner, pet) => new { Owner = owner, Pet = pet })
                                .Where(ownerPet => ownerPet.Pet.Type == "Cat")
                                .GroupBy(ownerPet => ownerPet.Owner.Gender, ownerPet => ownerPet.Pet.Name);

            _logger.LogInformation("Put grouped entries into result set.");
            return catGroupings.Select(x => new GenderCats
                                {
                                    OwnderGender = x.Key,
                                    CatNames = x.OrderBy(x => x).ToList()
                                }).ToList();
        }
    }
}
