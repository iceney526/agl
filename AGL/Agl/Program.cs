﻿using Agl.DataSource;
using Agl.PetSorters;
using Agl.Services;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Serilog;
using Serilog.Core;
using System;
using System.IO;
using System.Threading.Tasks;

namespace Agl
{
    class Program
    {
        private static Logger _logger;

        /// <summary>
        /// The Main function that configures execution environment and trigger the actual application code.
        /// </summary>
        static async Task Main()
        {
            //
            // Configure logger using Serilog.
            //
            // Additional sinkers can be added and configured if different logging targets are needed.
            //
            _logger = new LoggerConfiguration()
                            .WriteTo.File("agl.log")
                            .Enrich.FromLogContext()
                            .MinimumLevel.Information()
                            .CreateLogger();

            try
            {
                // Create service collection and configure the DI container.
                var services = ConfigureServices();

                // Generate a service provider instance.
                var serviceProvider = services.BuildServiceProvider();

                // Execute the actual code
                await serviceProvider.GetService<ConsoleApp>().Run();
            }
            catch (Exception ex)
            {
                // Exceptions occurred during initialization and executions will be caught and logged here.
                _logger.Error(ex, "Error occurred when running the application.");
            }
        }

        private static IServiceCollection ConfigureServices()
        {
            IServiceCollection services = new ServiceCollection();

            //
            // Load configurations from appsettings.json
            //
            var configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json")
                .Build();

            services.AddSingleton<IConfiguration>(configuration);

            //
            // Add Serilog instance as the logger.
            //
            services.AddLogging(cfg =>
            {
                cfg.AddSerilog(_logger);
            });

            //
            // Add necessary implementation / instances into the DI container.
            //
            services.AddTransient<IConfigService, LocalConfigService>();
            services.AddHttpClient<IDataSource, WebDataSource>();
            services.AddTransient<CatSorter>();
            services.AddTransient<ConsoleApp>();

            return services;
        }
    }
}
