﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Agl.Services.Models
{
    public class WebDataSourceConfig
    {
        public string DataUrl { get; set; }
    }
}
