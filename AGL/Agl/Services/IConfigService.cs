﻿using Agl.Services.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Agl.Services
{
    /// <summary>
    /// An interface that provides configuration service.
    /// </summary>
    public interface IConfigService
    {
        /// <summary>
        /// Gets configurations for the Web data source.
        /// </summary>
        /// <returns>Returns a WebDataSourceConfig object.</returns>
        WebDataSourceConfig GetWebDataSourceConfig();
    }
}
