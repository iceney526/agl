﻿using Agl.Services.Models;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Text;

namespace Agl.Services
{
    /// <summary>
    /// An implementation of IConfigService that retrieves configurations from local settings file.
    /// </summary>
    public class LocalConfigService : IConfigService
    {
        private ILogger _logger;
        private IConfiguration _configuration;

        /// <summary>
        /// Constructs a LocalConfigService instance with injected ILogger instance and .NET Core's IConfiguration instance.
        /// </summary>
        /// <param name="logger">The logger instance.</param>
        /// <param name="configuration">.NET Core's IConfiguration instance.</param>
        public LocalConfigService(ILogger<LocalConfigService> logger, IConfiguration configuration)
        {
            _logger = logger;
            _configuration = configuration;
        }

        /// <summary>
        /// Gets configurations for the Web data source.
        /// </summary>
        /// <returns>Returns a WebDataSourceConfig object.</returns>
        public WebDataSourceConfig GetWebDataSourceConfig()
        {
            _logger.LogInformation("Retrieve WebDataSource configuration from local settings.");

            return _configuration.GetSection("DataSource:Web").Get<WebDataSourceConfig>();
        }
    }
}
