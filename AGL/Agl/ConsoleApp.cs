﻿using Agl.PetSorters;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Agl
{
    /// <summary>
    /// The ConsoleApp class has the actual application execution code.
    /// </summary>
    public class ConsoleApp
    {
        private CatSorter _catSorter;

        /// <summary>
        /// Constructs a ConsoleApp instance using a CatSorter object.
        /// </summary>
        /// <param name="catSorter">An instance of the CatSorter class.</param>
        public ConsoleApp(CatSorter catSorter)
        {
            _catSorter = catSorter;
        }

        /// <summary>
        /// Uses the CatSorter instance to filter and sort pets, and output result to console.
        /// </summary>
        public async Task Run()
        {
            var genderCatSets = await _catSorter.Sort();

            foreach (var genderCats in genderCatSets)
            {
                Console.WriteLine(genderCats.OwnderGender);
                
                foreach (var cat in genderCats.CatNames)
                {
                    Console.WriteLine($"\t- {cat}");
                }

                Console.WriteLine();
            }
        }
    }
}
