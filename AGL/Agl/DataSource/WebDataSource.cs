﻿using Agl.Models;
using Agl.Services;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Agl.DataSource
{
    /// <summary>
    /// An implementation of IDataSource that gets data from a web service.
    /// </summary>
    public class WebDataSource : IDataSource
    {
        private readonly ILogger<WebDataSource> _logger;
        private HttpClient _httpClient;
        private IConfigService _configService;

        /// <summary>
        /// Constructs a WebDataSource instance using injected dependencies.
        /// </summary>
        /// <param name="logger">The ILogger instance.</param>
        /// <param name="httpClient">The HttpClient instance.</param>
        /// <param name="configService">The IConfigService instance.</param>
        public WebDataSource(ILogger<WebDataSource> logger, HttpClient httpClient, IConfigService configService)
        {
            _logger = logger;
            _configService = configService;

            httpClient.Timeout = TimeSpan.FromMinutes(1);
            _httpClient = httpClient;
        }

        /// <summary>
        /// Retrieves the data from the web data source.
        /// </summary>
        /// <returns>Returns a PetOwnership object.</returns>
        public async Task<PetOwnership> GetPetOwnershipAsync()
        {
            var config = _configService.GetWebDataSourceConfig();

            _logger.LogInformation($"Download data from {config.DataUrl}");
            var response = await _httpClient.GetStringAsync(config.DataUrl);

            return new PetOwnership { Owners = JsonConvert.DeserializeObject<List<Owner>>(response) };
        }
    }
}
