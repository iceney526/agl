﻿using Agl.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Agl.DataSource
{
    /// <summary>
    /// An interface that provides data to consumers.
    /// </summary>
    public interface IDataSource
    {
        /// <summary>
        /// Gets data that has pets and their owners information.
        /// </summary>
        /// <returns>Returns a PetOwnership object.</returns>
        Task<PetOwnership> GetPetOwnershipAsync();
    }
}
