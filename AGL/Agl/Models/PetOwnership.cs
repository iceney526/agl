﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Agl.Models
{
    public class PetOwnership
    {
        public List<Owner> Owners { get; set; }
    }
}
