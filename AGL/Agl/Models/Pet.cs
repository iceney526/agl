﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Agl.Models
{
    public class Pet
    {
        public string Name { get; set; }

        public string Type { get; set; }
    }
}
